**checkm8-icloud** 
Info on how iCloud activation works, gathered through research.
The checkm8 exploit by axi0mx written in Python 2.7 has been finally utilised in a public semi-tethered jailbreak called checkra1n. The checkm8 exploit is a permanent unpatchable bootrom exploit for all iOS devices up to iPhone X. This can be directly used (as you can see here) to avoid the iCloud activation lock, as the checkra1n jailbreak boots the device on a pwned DFU mode. This gives us the chance to start a ssh session, remount the root filesystem, and delete the Setup.app that gets appeared on top of springboard on the first boot of a iOS device.

**unlock**

In order to actually free the device from the previous owner, I won't explain the process fully but rather how the iCloud activation works as to my understanding. 

The serial number, and IMEI numbers (that need to be changed) are written in the mass memory and the baseband IC respectively. 
More specifically, on the mass memory, these info reside on the sysCfg which is a section of the S5L NOR. 
The important thing is that iBoot needs to populate the DeviceTree with this info, so you can access this using a patched iBoot or a kernel hack.
On older iOS devices, this was also possible by dumping the /dev/kmem virtual device, a process no longer possible, as the kernel no longer creates this.
To have a memory map in mind, the sysCfg Section resides on the 0x4000 offset till the 0x8000 where the Images Section starts. A copy of the SysCfg resides on the 0xFC000 region, as a copy that iBoot is going to use.  
The baseband  follows a different layout. The seczone has the IMEI signature at mem adress 0xA00 and the actual IMEI at 0xB00. The lock state is stored in the zone but the good thing is the locks table is identical for every device. The bad thing is the baseband processor has its own RAM and firmware in this NOR flash separate from the ARM CPU.
In case of a jailbreak however (not necessarily in the common meaning of having a graphical package manager in place - rather simply the ability to mount the root filesystem as read/write) we could, ssh into the iPhone and connect to the serial device the baseband CPU creates, like this `minicom -s`. The actual serial device is usually `/dev/tty.debug`. Then you could use some baseband commands in the `at` form like `at&h`. At this moment, you directly talk to the modem of your phone, simply put. 
Speaking again about the activation, I have found we can use the activation records from another iPhone (preferably same model number/carrier) to activate the baseband, even if the phone is unactivated. You can find these under the `needed_files` text. 
Keep in mind, the mobileactivation daemon may change the UUID of its folder, upon reboot!
With this approach, I have gone to the point of
-> Bypassing iCloud Activation
-> Getting SIM Card to work and cellular data

TL;DR Want to free an iPhone from iCloud? 

1) NAND( SysCfg region) read & write
2) BaseBand (?) read & write
3) Possibly other things I haven't found out.


